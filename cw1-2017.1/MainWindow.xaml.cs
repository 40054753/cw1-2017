﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cw1_2017._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    ///
    //author: Sean Faughey
    //Class purpose: main gui
    //Date last edited: 16/10/2016 - 00:29
    //
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            cbRegType.Items.Add("Attendee"); //add combobox items
            cbRegType.Items.Add("Student");
            cbRegType.Items.Add("Organiser");
        }
         
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            try //create attendee and check nothing is null
            {
                Attendee attendee = new Attendee(tbFirstName.Text, tbSecondName.Text, Int32.Parse(tbAttRef.Text), tbConName.Text, (bool) checkBoxPresenter.IsChecked
            , (bool) checkBoxPaid.IsChecked, cbRegType.Text, tbPaperTitle.Text);
                attendee.isValid();
                Institution institution = new Institution(tbInstName.Text);
                
            }

            catch (FormatException)
            {
            }
            
        }

        //if the attendee is a presenter, enable/disable paper title
        private void checkBoxPresenter_Checked(object sender, RoutedEventArgs e)
        {
            tbPaperTitle.IsEnabled = true;
        }

        private void checkBoxPresenter_Unchecked(object sender, RoutedEventArgs e)
        {
            tbPaperTitle.IsEnabled = false;
            tbPaperTitle.Text = "";
        }

        //clears the form
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            tbFirstName.Text = "";
            tbSecondName.Text = "";
            tbAttRef.Text = "";
            tbConName.Text = "";
            tbInstName.Text = "";
            tbPaperTitle.Text = "";
            cbRegType.SelectedIndex = -1;
            checkBoxPaid.IsChecked = false;
            checkBoxPresenter.IsChecked = false;
        }

        //shows the attendee's cost
        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Attendee attendee = new Attendee(tbFirstName.Text, tbSecondName.Text, Int32.Parse(tbAttRef.Text), tbConName.Text, (bool)checkBoxPresenter.IsChecked
            ,(bool) checkBoxPaid.IsChecked, cbRegType.Text, tbPaperTitle.Text);
                MessageBox.Show("The cost for this attendee is: " + attendee.getCost());
            }

            catch (FormatException)
            {
            }
        }
    }
}
