﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1_2017._1
{
    //author: Sean Faughey
    //Class purpose: institution object
    //Date last edited: 16/10/2016 - 00:29
    //
    class Institution
    {
        private string institutionName;
        public string institutionAddress;

        public Institution(string institutionName)
        {
            this.institutionName = institutionName;
        }

        public string getIAddress()
        {
            if (institutionName == "Napier") {
                institutionAddress = "Napier's Address";
            }
            if (institutionName == "NASA") {
                institutionAddress = "America";
            }

            return institutionAddress;
        }

    }
}
