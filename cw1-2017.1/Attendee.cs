﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace cw1_2017._1
{
    //author: Sean Faughey
    //Class purpose: Provides the majority of the functionality, particularly with attendee logic
    //Date last edited: 16/10/2016 - 00:29
    //
    class Attendee : Person
    {
        private int attendeeRef;
        private string conferenceName;
        private bool presenter;
        private bool paid;
        private string paperTitle;
        private string registrationType;
        private string firstName;
        private string secondName;

        //standard constructor
        public Attendee(string firstName, string secondName, int attendeeRef, string conferenceName, bool presenter, bool paid, string registrationType, string paperTitle)
        {
            this.firstName = firstName;
            this.secondName = secondName;
            this.attendeeRef = attendeeRef;
            this.conferenceName = conferenceName;
            this.presenter = presenter;
            this.paid = paid;
            this.registrationType = registrationType;
            this.paperTitle = paperTitle;
        }

        public void isValid()
        {
            try //VALIDITY CHECK - firstName
            {
                if (firstName == "")
                {
                    throw new ArgumentException("You must enter a first name!");
                }
            }

            catch (Exception excep)
            {
                Console.WriteLine(excep.Message);
            }

            try //VALIDITY CHECK - secondName
            {
                if (secondName == "")
                {
                    throw new ArgumentException("You must enter a second name!");
                }
            }

            catch (Exception excep)
            {
                Console.WriteLine(excep.Message);
            }

            try //VALIDITY CHECK - attendeeRef
            {
                if (attendeeRef <40000 || attendeeRef > 60000)
                {
                    throw new ArgumentException("You must enter a valid attendee reference number! (Range 40000 - 60000)");
                }
            }

            catch (Exception excep)
            {
                Console.WriteLine(excep.Message);
            }

            try //VALIDITY CHECK - conferenceName
            {
                if (conferenceName == "")
                {
                    throw new ArgumentException("You must enter a conference name!");
                }
            }

            catch (Exception excep)
            {
                Console.WriteLine(excep.Message);
            }

            try //VALIDITY CHECK - registrationType
            {
                if (registrationType == "")
                {
                    throw new ArgumentException("You must enter a registration type!");
                }
            }

            catch (Exception excep)
            {
                Console.WriteLine(excep.Message);
            }

            try //VALIDITY CHECK - paperTitle
            {
                if (presenter && paperTitle == "")
                {
                    throw new ArgumentException("You must enter a paper title!");
                }
            }

            catch (Exception excep)
            {
                Console.WriteLine(excep.Message);
            }
        }

        // returns the cost for this attendee as a double !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! would like to see if can make an int
        public double getCost()
        {
            int full = 500;
            int student = 300;
            double total = 0;

            if (registrationType == "Attendee") {
                total = full;
            }

            if (registrationType == "Student") {
                total = student;
            }

            //if the attendee is a presenter, apply 10% discount
            if (presenter) {
                total = total*0.9;
            }

            return total;
        }

    }
}
